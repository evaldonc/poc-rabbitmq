package com.example.demo;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Send {

	private final static String QUEUE_NAME = "FilaTeste2";

	public static void main(String[] argv) throws Exception {
		String msg = "mensagem nª ";

		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("10.10.2.196");
		factory.setPort(5672);
		//        factory.setHost("localhost");
		//        factory.setPort(8080);
		factory.setUsername("guest");
		factory.setPassword("guest");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		for (int i = 0; i < 100000; i++) {
			publisher(msg + i, factory, connection, channel);
		}

		channel.close();
		connection.close();

	}

	public static void publisher(String msg, ConnectionFactory factory, Connection connection, Channel channel)
			throws IOException, TimeoutException {

		try {
			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
			String message = msg;
			channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
			System.out.println(" [x] Sent '" + message + "'");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}